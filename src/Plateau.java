import javafx.util.Pair;

import java.util.ArrayList;
import java.util.Random;

public class Plateau{
    private int width, height;
    private Cell [][] map;
    private   ArrayList<Pair<Integer, Integer>> coordAround;
    public Plateau(int x, int y){

        this.coordAround = new ArrayList<>();
        this.coordAround.add(new Pair<>(0, 0));

        this.coordAround.add(new Pair<>(0, 1));
        this.coordAround.add(new Pair<>(0, -1));
        this.coordAround.add(new Pair<>(-1, 0));
        this.coordAround.add(new Pair<>(1, 0));

        this.coordAround.add(new Pair<>(1, 1));
        this.coordAround.add(new Pair<>(1, -1));
        this.coordAround.add(new Pair<>(-1, 1));
        this.coordAround.add(new Pair<>(-1, -1));




        this.width=x;
        this.height=y;
        this.map=new Cell[x][y];
        for(int i=0; i<x; ++i){
            for(int j=0; j<y; ++j){
                this.map[i][j] = new Cell(i,j);
            }
        }
    }

    public Cell getCell(int pos){
        int x = pos % this.width ;
        int y = (pos - x )  / this.width;
        return this.map[x][y];
    }
    public  Cell[][] getMap(){
        return this.map;
    }
    public int getWidth(){
        return this.width;
    }
    public int getHeight(){
        return this.height;
    }



    /* Difficulty c'est une pourcentage de rappartition des bombes
     * x,y c'est l'endroit ou le joueur à cliqué, on veut pas y mettre des bombes
     * Cette fonction modifie directement le plateau pas besoin de renvoyer quelque choses
     * Il faut penser que cette fonction doit aussi actualiser les cellules avec le nombre de bombe à poximité
     * Renvois le nombre de bomb placé
     */
    public int placeBomb(int difficulty, int x, int y){

        int nombreBomb = 0;
        ArrayList<Pair<Integer, Integer>> indexPossible = new ArrayList<>();
        for(int i=0; i<this.map.length; ++i){
            for(int j=0; j<this.map[i].length; ++j){
                indexPossible.add(new Pair<>(i,j));
            }
        }



        int freeZone = 9;

        for(Pair<Integer,Integer> p : this.coordAround){
            if(this.indexValid(x+p.getKey(), y+p.getValue())){
                indexPossible.remove(new Pair<>(x+p.getKey(),y+p.getValue()));
            }else if (x+p.getKey() != x || y+p.getValue() != y) freeZone--;

        }

        if (difficulty >100) difficulty = 100;
        int bombQuantity =  (int) ((width*height - freeZone ) * difficulty/100 );

        while (bombQuantity > 0 ){
                Random rand = new Random();
                int rands = rand.nextInt(indexPossible.size());
                Pair<Integer, Integer> cellPossible = indexPossible.remove(rands);
                bombQuantity--;
                this.getCell(cellPossible.getKey(), cellPossible.getValue()).setBomb();
                nombreBomb++;

            }

        this.calcultateNearBombNumber();
        return  nombreBomb;
    }
    private boolean indexValid(int x, int y){
        return (0 <= x &&  x < this.width && 0 <= y &&  y < this.height);
    }
    private void calcultateNearBombNumber(){
        for(int i = 0; i < this.map.length; i++){
            for(int j = 0; j < this.map[i].length; j++){
                this.calcultateNearBombNumberOfOneCell(this.map[i][j]);
            }
        }
    }
    public void showBomb(){
        for(int i = 0; i < this.map.length; i++){
            for(int j = 0; j < this.map[i].length; j++){
                if (this.map[i][j].isBombed()){
                    this.map[i][j].show();
                }
            }
        }
    }

    public boolean isWin(){
        boolean res = true;
        for(int i = 0; i < this.map.length; i++){
            for(int j = 0; j < this.map[i].length; j++){
                if (this.map[i][j].isBombed() && this.map[i][j].getMarking() != 1){
                    res = false;
                }
            }
        }
        return res;
    }
    public void explorer(int x, int y){
        Cell cF = this.getCell(x, y);
        if(cF.getNear() != 0 && cF.isHidden()){
            cF.show();
        }else{
            for (Pair<Integer, Integer> p : this.coordAround){
                if (this.indexValid(x+p.getKey(), y+p.getValue())){
                    Cell c = this.getCell(x+p.getKey(), y+p.getValue());
                    if (c.isHidden() && c.getMarking()==0){
                        c.show();
                        if(c.getNear() == 0){
                            this.explorer(x+p.getKey(), y+p.getValue());
                        }
                    }

                }
            }
        }


    }
    public boolean thePartyIsOver(){
        for(int i = 0; i < this.map.length; i++){
            for(int j = 0; j < this.map[i].length; j++){
                if(this.map[i][j].isLose()){
                    return true;
                }
            }
        }
        return  false;
    }
    private void calcultateNearBombNumberOfOneCell(Cell c){

        int x = c.getX();
        int y = c.getY();
        int neighboringBombs = 0;

        for (Pair<Integer, Integer> p : this.coordAround){
            if (this.indexValid(x+p.getKey(), y+p.getValue())){
                if (this.getCell(x+p.getKey(), y+p.getValue()).isBombed()){
                    neighboringBombs++;
                }

            }
        }
        //c.show();
        c.setNear(neighboringBombs);
    }
    public int howManyMarkAround(int x, int y){
        int neighboringMarks = 0;

        for (Pair<Integer, Integer> p : this.coordAround){
            if (this.indexValid(x+p.getKey(), y+p.getValue())){
                if (this.getCell(x+p.getKey(), y+p.getValue()).getMarking() != 0){
                    neighboringMarks++;
                }

            }
        }
        //c.show();
        return  neighboringMarks;
    }
    public Cell getCell(int x, int y){

        return this.map[x][y];
    }
    ///** Méthode d'affichage du plateau */
    //public String toString(){
    //    String repr=new String();
    //    for(int i=0; i<this.width*this.height; ++i){
    //        if(i!=0 && i%this.width==0){
    //            repr+="\n";
    //        }
    //        if(!this.getCarte(i).isShowed()){
    //            repr+="| O ";}
    //        else{repr+="| "+this.getCarte(i).getNb()+" ";}
    //    }
    //    return repr;
    //}
}