public class Carte{
    private boolean mine, show;
    private int number;
    private Boolean flag; // Comme ça on à 3 valeurs pour le flag
    public Carte(){
        this.mine=false;
        this.flag=null;
        this.show=false;
        this.number=0;
    }
    // Getters
    /** Retourne true si la case est révélée, false sinon */
    public boolean isShowed(){return this.show;}
    /** Retourne le nombre de mine aux alentours */
    public int getNb(){return this.number;}

    // Setters
    /** Pose ou enlève un drapeau ou un '?' sur la case ciblée */
    public void setFlag(){
        if(this.flag==null && !this.show){this.flag=false;} // Pose un drapeau
        else{
            if(!this.flag){this.flag=true;} // Pose un '?'
            else{this.flag=null;} // Enlève le drapeau et le '?'
        }
    }
    /** Révèle la case ciblée */
    public void show(){
        if(this.flag==null){this.show=true;}
    }
}
