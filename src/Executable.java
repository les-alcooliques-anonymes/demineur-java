import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;

public class Executable extends Application {

    protected Plateau plateau;
    protected int cellSizePx = 30;
    protected int marge = 100;

    protected Integer nbBomb = 0;
    protected boolean thePartyIsOver = false;
    protected List<Image> nearNumber,marking;
    protected Image bomb,cell,bombAlpha;

    protected boolean firstClickIsDone = false;

    public static void main(String[] args) {
        launch(args);
    }

    private void drawAllCell(GraphicsContext gc){
        for(int i = 0; i < this.plateau.getMap().length; i++){
            for(int j = 0; j < this.plateau.getMap()[i].length; j++){
                this.drawOneCell(gc, this.plateau.getMap()[i][j]);
            }
        }

    }
    private void drawGui(GraphicsContext gc){
        gc.setFill(Color.WHITE);
        gc.fillRect(0,0, this.marge,512);
        gc.setFill(Color.BLACK);
        for(int j=0; j<3; ++j){
            gc.drawImage( this.bombAlpha, (int) 10+ cellSizePx*0.90*j, 10);
        }
        Font theFont = Font.font( "Times New Roman", FontWeight.BOLD, cellSizePx );
        gc.setFont( theFont );
        gc.fillText( this.nbBomb.toString(), 10,  10+ cellSizePx*2 );
        gc.strokeText( this.nbBomb.toString(), 10,  10+ cellSizePx*2 );
    }

    /* Actualise / place un cellule dans le cavena */
    private void drawOneCell(GraphicsContext gc, Cell c){
        if(c.isHidden()){
            gc.drawImage( this.marking.get(c.getMarking()), c.getX()*this.cellSizePx+this.marge, c.getY()*this.cellSizePx);
        }else{
            if(c.isBombed()){
                gc.drawImage( this.bomb, c.getX()*this.cellSizePx+this.marge, c.getY()*this.cellSizePx);
            }else{

                gc.drawImage( this.nearNumber.get(c.getNear()), c.getX()*this.cellSizePx+this.marge, c.getY()*this.cellSizePx);
            }

        }


    }


    /* Fonction qui est appelé à chaque click de souris */
    private void clickEventInGame(int x, int y, String click, GraphicsContext gc){
        if (!this.thePartyIsOver){
            if (click == "PRIMARY"){
                if (!firstClickIsDone){

                    this.nbBomb = this.plateau.placeBomb(13,x,y);
                    this.drawGui(gc);
                    this.plateau.explorer(x,y);
                    this.drawAllCell(gc);
                    firstClickIsDone = true;
                }
                else{
                    Cell selectedCell = this.plateau.getCell(x,y);

                    if (selectedCell.isHidden()){
                        if (selectedCell.mark()==2){
                            this.nbBomb ++;
                            this.drawGui(gc);
                        }
                        this.plateau.explorer(x,y);
                        this.drawAllCell(gc);
                    }

                }
            }
            else if (click == "SECONDARY" && this.firstClickIsDone){
                Cell selectedCell = this.plateau.getCell(x,y);
                //System.out.println(x+","+y+" - "+selectedCell.getX()+","+selectedCell.getY());
                if (selectedCell.isHidden()){
                    int r = selectedCell.mark();
                    if (r == 1){
                        this.nbBomb --;
                    }else if (r==2){
                        this.nbBomb ++;
                    }

                    this.drawGui(gc);
                    this.drawOneCell(gc, selectedCell);


                    if (this.nbBomb==0 ){
                        if (this.plateau.isWin()){
                            Font theFont = Font.font( "Times New Roman", FontWeight.BOLD, 100 );
                            gc.setFont( theFont );
                            gc.fillText( "VICTORY", 60+this.marge, 100 );
                            gc.strokeText( "VICTORY !", 60+this.marge, 100 );
                            this.thePartyIsOver = true;
                        }
                    }
                }
                else{
                    if( this.plateau.howManyMarkAround(x,y) >= selectedCell.getNear()){
                        this.plateau.explorer(x,y);
                        this.drawAllCell(gc);
                    }

                }

            }
            if (this.plateau.thePartyIsOver()){
                this.plateau.showBomb();
                this.drawAllCell(gc);
                Font theFont = Font.font( "Times New Roman", FontWeight.BOLD, 100 );
                gc.setFont( theFont );
                gc.fillText( "GAME OVER !", 60+this.marge, 100 );
                gc.strokeText( "GAME OVER !", 60+this.marge, 100 );
                this.thePartyIsOver = true;
            }
        }else{
            this.firstClickIsDone = false;
            this.thePartyIsOver = false;
            this.plateau=new Plateau(30,15);
            this.drawAllCell(gc);
        }

    }


    private void importImage(){
        this.nearNumber = new ArrayList<Image>();
        for(int j=0; j<9; ++j){
            this.nearNumber.add(new Image( "img/"+j+".png",this.cellSizePx,this.cellSizePx,false,false));
        }
        this.marking = new ArrayList<Image>();
        this.marking.add(new Image( "img/cell.png",this.cellSizePx,this.cellSizePx,false,false));
        this.marking.add(new Image( "img/mark2.png",this.cellSizePx,this.cellSizePx,false,false));
        this.marking.add(new Image( "img/unknow2.png",this.cellSizePx,this.cellSizePx,false,false));


        this.cell = new Image( "img/cell.png",this.cellSizePx,this.cellSizePx,false,false);
        this.bomb   = new Image( "img/bom.png",this.cellSizePx,this.cellSizePx,false,false );
        this.bombAlpha   = new Image( "img/bomAlpha.png",this.cellSizePx,this.cellSizePx,false,false);
    }


    @Override
    public void start(Stage primaryStage) {

        this.plateau=new Plateau(30,15);
        primaryStage.setTitle( "Démineur" );

        Group root = new Group();
        Scene theScene = new Scene( root );
        primaryStage.setScene( theScene );

        Canvas canvas = new Canvas( this.plateau.getWidth()*this.cellSizePx+this.marge, this.plateau.getHeight()*this.cellSizePx );
        root.getChildren().add( canvas );

        GraphicsContext gc = canvas.getGraphicsContext2D();

        theScene.setOnMouseClicked(
                new EventHandler<MouseEvent>()
                {
                    public void handle(MouseEvent e)
                    {
                        //System.out.println( e.getX()+", "+ e.getY() + "click  : " + e.getButton());
                        clickEventInGame((int) ( (e.getX()-marge) / cellSizePx ),  (int) (e.getY() / cellSizePx ),e.getButton().toString(), gc );
                    }
                });



        this.importImage();
        this.drawAllCell(gc);

       // final long startNanoTime = System.nanoTime();

        //new AnimationTimer()
        //{
        //    public void handle(long currentNanoTime)
        //    {
        //        /* Boucle du jeu s'éxecute 60 fois par seconde environ*/
        //        double t = (currentNanoTime - startNanoTime) / 1000000000.0;
        //
        //    }
        //}.start();

        primaryStage.show();
    }
}
