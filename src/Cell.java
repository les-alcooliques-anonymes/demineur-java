public class Cell {
    private boolean bomb, hidden;
    private int near, marking, x, y;
    public Cell(int x, int y){
        this.bomb=false;
        this.hidden=true;
        this.near=0;
        this.marking=0;

        /* Coordonnées de la cellule en question */
        this.x = x;
        this.y = y;
    }
    public int getX(){
        return this.x;
    }
    public int getY(){
        return this.y;
    }
    public boolean isHidden(){
        return this.hidden;
    }
    public boolean isBombed(){
        return this.bomb;
    }
    public void setBomb(){
        this.bomb = true;
        this.near = -1;
    }
    public void setNear(int number){
        this.near = number;
    }


    public void show(){
        this.hidden=false;
    }


    /*
     * 0 is nothing
     * -1 is nothing too
     * 1 is the flag
     * 2 is the ?
     */
    public int mark(){
        this.marking++;
        if  (this.marking>2){
            this.marking=0;
        }
        return  this.marking;
    }


    public int getMarking(){
        return this.marking;
    }
    public int getNear(){
        return this.near;
    }
    public boolean isLose(){
        if(this.bomb && !this.hidden){
            return true;
        }
        else {
            return false;
        }
    }





    // Getters
    ///** Retourne true si la case est révélée, false sinon */
    //public boolean isShowed(){return this.show;}
    ///** Retourne le nombre de mine aux alentours */
    //public int getNb(){return this.number;}

    // Setters
    ///** Pose ou enlève un drapeau ou un '?' sur la case ciblée */
    ///public void setFlag(){
    //    if(this.flag==null && !this.show){this.flag=false;} // Pose un drapeau
    //    else{
    //        if(!this.flag){this.flag=true;} // Pose un '?'
    //        else{this.flag=null;} // Enlève le drapeau et le '?'
    //    }
    //}
    ///** Révèle la case ciblée */
    //public void show(){
     //   if(this.flag==null){this.show=true;}
    //}
}
